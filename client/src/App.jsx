import React from 'react';
import './App.css';
// export { DataStatus } from './data-status.enum';
import Chat from './bsa';
import { Route, Routes } from 'react-router-dom';
import { AppPath } from './common/enums/app/app-path.enum';
import AuthPage from './components/authPage/AuthPage';
import UsersPage from "./components/usersPage/UsersPage";

function App() {
  return (
    <div className="app relative">
      <Routes>
        <Route path={AppPath.LOGIN} element={<AuthPage />} />
        <Route path={AppPath.USERS} element={<UsersPage />} />
        <Route path={AppPath.CHAT} element={<Chat />} />
      </Routes>
    </div>
  );
}

export default App;
