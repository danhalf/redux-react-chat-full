import { ApiPath, HttpMethod } from '../../common/enums/enums';
import { ContentType } from "../../common/enums/file/file.enum";
import { MessagesKey } from "../../common/enums/enums";
import { getISODate, getRandomId } from "../../Helpers/helpers";

class Messages {
    constructor({ baseUrl, http }) {
        this._baseUrl = baseUrl;
        this._http = http;
        this._basePath = ApiPath.MESSAGES;
    }

    getAll() {
        return this._http.load(this._getUrl(), {
            method: HttpMethod.GET,
        });
    }

    createMessage(payload) {
        const message = {
            [MessagesKey.ID]: getRandomId(),
            [MessagesKey.TEXT]: payload,
            [MessagesKey.CREATED]: getISODate(),
            [MessagesKey.LIKED]: [],
            [MessagesKey.EDITED]: '',
            [MessagesKey.USER_ID]: 'uid',
            [MessagesKey.USER_NAME]: 'uname',
            [MessagesKey.AVATAR]: 'u_avatar',
        }
        return this._http.load(this._getUrl(), {
            method: HttpMethod.POST,
            contentType: ContentType.JSON,
            payload: JSON.stringify(message),
        });
    }

    updateMessage(payload) {
        const {id, text} = payload;
        const updatedMessage = {
            [MessagesKey.ID]: id,
            [MessagesKey.TEXT]: text,
            [MessagesKey.EDITED]: getISODate(),
        }
        return this._http.load(this._getUrl(id), {
            method: HttpMethod.PUT,
            contentType: ContentType.JSON,
            payload: JSON.stringify(updatedMessage),
        });
    }

    likeMessage(payload) {
        const {messageId, currentUserId} = payload;
        const updatedMessage = {
            [MessagesKey.LIKED]: currentUserId,
        }
        return this._http.load(this._getUrl(messageId), {
            method: HttpMethod.PUT,
            contentType: ContentType.JSON,
            payload: JSON.stringify(updatedMessage),
        });
    }

    deleteMessage(id) {
        return this._http.load(this._getUrl(id), {
            method: HttpMethod.DELETE,
        });
    }


    _getUrl(path = '') {
        return `${ this._baseUrl }${ this._basePath }/${ path }`;
    }
}

export { Messages };
