import { ENV } from '../common/enums/enums';
import { Http } from './http/http.service';
import { Messages } from './messages/messages.service';
import { Users } from './users/users.service';

const http = new Http();
const messages = new Messages({
  baseUrl: ENV.API.URL,
  http,
});

const users = new Users({
  baseUrl: ENV.API.URL,
  http,
});

export { http, messages, users };
