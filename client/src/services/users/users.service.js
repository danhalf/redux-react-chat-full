import { ApiPath, HttpMethod, UsersKey } from '../../common/enums/enums';
import { getRandomId } from "../../Helpers/getRandomId";
import { getISODate } from "../../Helpers/getTime";
import { ContentType } from "../../common/enums/file/content-type.enum";

class Users {
  constructor({ baseUrl, http }) {
    this._baseUrl = baseUrl;
    this._http = http;
    this._basePath = ApiPath.USERS;
  }

  getAll() {
    return this._http.load(this._getUrl(), {
      method: HttpMethod.GET,
    });
  }

  getUser(id) {
    return this._http.load(`${ this._getUrl() }/login`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(id),
    });
  }

  createUser(payload) {
    const {user, password, avatar} = payload;
    const newUser = {
      [UsersKey.CREATED]: getISODate(),
      [UsersKey.EDITED]: '',
      [UsersKey.USER_ID]: getRandomId(),
      [UsersKey.ROLE]: 'member',
      [UsersKey.USER_NAME]: user,
      [UsersKey.PASSWORD]: password,
      [UsersKey.AVATAR]: avatar,
    }
    return this._http.load(this._getUrl(), {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(newUser),
    });
  }


  _getUrl(path = '') {
    return `${this._baseUrl}${this._basePath}/${path}`;
  }
}

export { Users };
