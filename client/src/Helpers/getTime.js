const getISODate = () => new Date().toISOString()

const getTime = fullDate => {
    const date = new Date(fullDate);
    return `${ date.getHours().toString().padStart(2, 0) }:${ date.getMinutes().toString().padStart(2, 0) }`
}

const getMessageDay = (messageDate) => {
    const date = new Date(messageDate);
    const today = new Date().getDate() - date.getDate() === 0 && 'today'
    const yesterday = new Date().getDate() - date.getDate() === 1 && 'yesterday'
    const month = date.toLocaleString('en-GB', { month: 'long' })
    const fullDate = [date.getFullYear(), month, date.getDate()].join(' ')
    return `${ today || yesterday || fullDate }`;
};

const getLastMessageTime = (messagesState) => {
    const [ lastMessageData ] = Object.values(messagesState).map(res=> {
        //TODO ref this func
        const date = new Date(res.createdAt)
        return date.getTime()
    }).sort((a, b)=> b - a);
    const lastDate = new Date(lastMessageData)
    return `${lastDate.getDate()}.${lastDate.getMonth().toString().padStart(2, 0)}.${lastDate.getFullYear()} ${lastDate.getHours().toString().padStart(2, 0)}:${lastDate.getMinutes().toString().padStart(2, 0)}:${lastDate.getSeconds().toString().padStart(2, 0)}`
}

export {getISODate, getTime, getMessageDay, getLastMessageTime};