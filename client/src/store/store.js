import { configureStore } from '@reduxjs/toolkit';
import { messages as messagesService } from '../services/services';
import { users as usersService } from '../services/services';
import { messages, users } from "../bsa";

const store = configureStore({
        reducer: {
            messages,
            users,
    },
    middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
        thunk: {
            extraArgument: {
                messagesService,
                usersService
            },
        },
    }),
});

export { store };
