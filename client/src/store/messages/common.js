const ActionType = {
  FETCH_MESSAGES: 'messages/fetch-messages',
  LIKE: 'messages/like',
  ADD: 'messages/add',
  EDIT: 'messages/edit',
  DELETE: 'messages/delete',
};

export { ActionType };
