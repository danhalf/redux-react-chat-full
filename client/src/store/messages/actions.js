import { ActionType } from './common';

import { createAsyncThunk } from '@reduxjs/toolkit';

const fetchMessages = createAsyncThunk(ActionType.FETCH_MESSAGES, async (_args, { extra }) => ({
    messages: await extra.messagesService.getAll(),
}));

const addMessage = createAsyncThunk(ActionType.ADD, async (payload, { extra }) => ({
    message: await extra.messagesService.createMessage(payload),
}))

const editMessage = createAsyncThunk(ActionType.EDIT, async (payload, { extra }) => ({
    message: await extra.messagesService.updateMessage(payload),
}));

const likeMessage = createAsyncThunk(ActionType.LIKE, async (payload, { extra }) => ({
    message: await extra.messagesService.likeMessage(payload),
}));

const deleteMessage = createAsyncThunk(ActionType.DELETE, async (payload, { extra }) => ({
    message: await extra.messagesService.deleteMessage(payload),
}));



export {
    fetchMessages,
    addMessage,
    deleteMessage,
    likeMessage,
    editMessage,
};
