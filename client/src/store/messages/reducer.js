import { fetchMessages, addMessage, editMessage, deleteMessage, likeMessage } from './actions';
import { DataStatus } from '../../common/enums/enums';
import { createReducer } from "@reduxjs/toolkit";


const initialState = {
    chat: {
        messages: [],
        editModal: false,
        status: null,
        preloader: true,
    },
    status: DataStatus.IDLE,
};

const reducer = createReducer(initialState, builder => {
    builder.addCase(fetchMessages.pending, state => {
        state.status = DataStatus.PENDING;
    })

    builder.addCase(fetchMessages.fulfilled, (state, { payload }) => {
        const { messages } = payload;

        state.messages = messages;
        state.status = DataStatus.SUCCESS;
    });

    builder.addCase(addMessage.fulfilled, (state, { payload }) => {
        const { message } = payload;

        state.messages = state.messages.concat(message);
    });

    builder.addCase(editMessage.fulfilled, (state, { payload }) => {
        const { message } = payload;
        state.messages = state.messages.map((msg) => {
            return msg.id === message.id ? { ...msg, ...message } : msg;
        });
    });

    builder.addCase(likeMessage.fulfilled, (state, { payload }) => {
        const { message } = payload;
        const { id, liked: userId } = message;
        console.log(payload);
        state.messages = state.messages.map((msg) => {
            if (msg.id === id) {
                if (msg.liked.includes(userId)) {
                    console.log('log inc', msg.liked);
                    return msg.liked.filter(msg=> msg !== userId)
                }
                console.log('log notinc', msg.liked)
                return { ...msg.liked, ...userId }
            }
            return msg;
        });
    });

    builder.addCase(deleteMessage.fulfilled, (state, { payload }) => {
        const { message: messageId } = payload;
        state.messages = state.messages.filter((msg) => msg.id !== messageId);
    });


})

export { reducer };
