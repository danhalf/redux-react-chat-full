const ActionType = {
  SET_USERS: 'users/set-users',
  GET_USER: 'users/get-user',
  FETCH_USERS: 'users/fetch-users',
  ADD: 'users/add',
  EDIT: 'users/edit',
  DELETE: 'users/delete',
};

export { ActionType };
