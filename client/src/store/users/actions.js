import { ActionType } from './common';

import { createAsyncThunk } from '@reduxjs/toolkit';

const fetchUsers = createAsyncThunk(ActionType.FETCH_USERS, async (_args, { extra }) => ({
    users: await extra.usersService.getAll(),
}));

const addUser = createAsyncThunk(ActionType.ADD, async (payload, { extra }) => ({
    users: await extra.usersService.createUser(payload),
}))

const getUser = createAsyncThunk(ActionType.GET_USER, async (payload, { extra }) => ({
    users: await extra.usersService.getUser(payload),
}))

// const editUser = createAsyncThunk(ActionType.EDIT, async (payload, { extra }) => ({
//     message: await extra.usersService.changeUserData(payload),
// }));

// const deleteUser = createAsyncThunk(ActionType.DELETE, async (payload, { extra }) => ({
//     message: await extra.usersService.deleteUser(payload),
// }));



export {
    fetchUsers,
    addUser,
    getUser
};
