import { fetchUsers, addUser, getUser } from './actions';
import { DataStatus } from '../../common/enums/enums';
import { createReducer } from "@reduxjs/toolkit";

const initialState = {
  users: [
    {
      "userId": "res55",
      "userName": "Main_adm",
      "role": "admin"
    },
  ],
  status: DataStatus.IDLE,
};



const reducer = createReducer(initialState, builder => {
  builder.addCase(fetchUsers.pending, state => {
    state.status = DataStatus.PENDING;
  })

  builder.addCase(fetchUsers.fulfilled, (state, { payload }) => {
    const { users } = payload;
    state.users = users;
    state.status = DataStatus.SUCCESS;
  });

  builder.addCase(addUser.fulfilled, (state, { payload }) => {
    const { user } = payload;

    state.users = state.users.concat(user);
  });

  builder.addCase(getUser.fulfilled, (state, { payload }) => {
    const { user, role } = payload.users;
    // next line written in a hurry, sorry :(
    role === 'admin' ? window.location.href = '/users' : window.location.href = '/chat'
    state.users = state.users.concat(user);
  });



})

export { reducer };
