const ApiPath = {
  MESSAGES: '/api/messages',
  USERS: '/api/users',
};

export { ApiPath };
