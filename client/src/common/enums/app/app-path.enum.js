const AppPath = {
  // ROOT: '/',
  CHAT: '/chat',
  LOGIN: '/',
  REGISTRATION: '/registration',
  USERS: '/users',
  USERS_$ID: '/users/:id',
  ANY: '*',
};

export { AppPath };
