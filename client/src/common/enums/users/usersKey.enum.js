const UsersKey = {
    USER_ID: 'userId',
    AVATAR: 'avatar',
    USER_NAME: 'user',
    CREATED: 'createdAt',
    ROLE: 'role',
    PASSWORD: 'password',
    EDITED: 'editedAt'
};

export { UsersKey };