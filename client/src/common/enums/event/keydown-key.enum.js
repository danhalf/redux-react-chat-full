const KeydownKey = {
  ESCAPE: 'Escape',
  ENTER: 'Enter',
  TAB: 'Tab',
  UP: 'ArrowUp'
};

export { KeydownKey };