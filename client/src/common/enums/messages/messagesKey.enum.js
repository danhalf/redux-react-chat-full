const MessagesKey = {
    ID: 'id',
    USER_ID: 'userId',
    LIKED: 'liked',
    TEXT: 'text',
    AVATAR: 'avatar',
    USER_NAME: 'user',
    CREATED: 'createdAt',
    EDITED: 'editedAt'
};

export { MessagesKey };