import { useState } from "react";

const MessageInput = ({ onMessageAdd }) => {

    const [message, setMessage] = useState('');
    const [isDisable, setIsDisable] = useState(true)

    const changeMessage = (event) => {
        const msg = event.target.value;
        setMessage(msg);
        checkIsDisabled(msg)
    }

    const checkIsDisabled = (message) => message ? setIsDisable(false) : setIsDisable(true)

    const onMessageSend = () => {
        if (message) {
            onMessageAdd(message)
            setMessage('');
            setIsDisable(true)
        }
    }

    return (
        <div className="message-input mt-3 flex justify-center">
            <div className="w-2/4 xl:w-96">
                <textarea
                    className="
                        form-control
                        block
                        w-full
                        px-3
                        py-1.5
                        text-base
                        font-normal
                        text-gray-700
                        bg-white bg-clip-padding
                        border border-solid border-gray-300
                        rounded
                        transition
                        ease-in-out
                        m-0
                        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
                    "
                    id="exampleFormControlTextarea1"
                    rows="3"
                    placeholder="Your message"
                    value={message}
                    onChange={changeMessage}
                />
            </div>
            <div className="flex space-x-2 justify-center items-center">
                    <button type="button"
                            onClick={onMessageSend}
                            disabled={isDisable}
                            className="disabled:opacity-50 w-20 h-12 ml-3 inline-block px-6 pt-2.5 pb-2 bg-blue-600 text-white font-medium text-xs leading-normal uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out flex align-center">
                        <svg className="h-8 w-8 text-blue-400" width="24" height="24" viewBox="0 0 24 24"
                             strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round"
                             strokeLinejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"/>
                            <line x1="10" y1="14" x2="21" y2="3"/>
                            <path d="M21 3L14.5 21a.55 .55 0 0 1 -1 0L10 14L3 10.5a.55 .55 0 0 1 0 -1L21 3"/>
                        </svg>
                    </button>
            </div>
        </div>
    );
};

export default MessageInput;