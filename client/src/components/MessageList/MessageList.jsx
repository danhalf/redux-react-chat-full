import Message from './Message/Message';
import OwnMessage from './OwnMessage/OwnMessage';
import { useCallback, useEffect, useState } from 'react';
import EditMessageModal from '../EditMessageModal/EditMessageModal';
import { useDispatch } from 'react-redux';
import { messages as messagesActionCreator } from '../../store/actions';
import { getMessageDay } from '../../Helpers/helpers';
import { KeydownKey } from '../../common/enums/event/keydown-key.enum';

function MessageList({ messages, currentUserId, onMessageDelete }) {
  const [currentMessage, setCurrentMessage] = useState(null);
  const dispatch = useDispatch();
  const hasCurrentMessage = Boolean(currentMessage);

  const handleEditMessageModalClose = () => setCurrentMessage(null);

  const handleMessageSave = useCallback(
    (message) => {
      dispatch(messagesActionCreator.editMessage(message));
      setCurrentMessage(null);
    },
    [dispatch]
  );

  const handleMessageLike = useCallback(
      (messageId) => {
        dispatch(messagesActionCreator.likeMessage({ messageId, currentUserId }))
      }
  )

  const handleMessageEdit = (message) => setCurrentMessage(message);

  const groupMessageByDate = () => {
    const messageByDate = {};
    messages && Object.values(messages).forEach((item) => {
      const [date] = item.createdAt.split('T');
      if (!messageByDate[date]) {
        messageByDate[date] = [];
      }
      messageByDate[date].push(item);
    });
    return messageByDate;
  };

  const handleUpDown = useCallback(
    ({ key }) => {
      if (key === KeydownKey.UP) {
        setCurrentMessage(
          Object.values(messages)
            .filter(({ userId }) => userId === currentUserId)
            .map(({ id, text }) => ({ id, text }))
            .reverse()[0]
        );
      }
    },
    [currentUserId, messages]
  );

  useEffect(() => {
    document.addEventListener('keydown', handleUpDown);

    return () => {
      document.removeEventListener('keydown', handleUpDown);
    };
  }, [handleUpDown]);

  return (
    <section className="messages">
      {messages && Object.entries(groupMessageByDate()).map(([dateMessage, messages]) => {
        return (
          <ul key={dateMessage} className="message-list flex flex-col">
            <li className="bg-lime-200 h-14 text-center mt-8">
              {getMessageDay(dateMessage)}
            </li>
            {messages.map(
              ({ userId, id, text, createdAt, editedAt, avatar, user, liked }) =>
                userId === currentUserId ? (
                  <OwnMessage
                    key={id}
                    id={id}
                    text={text}
                    createdAt={createdAt}
                    editedAt={editedAt}
                    onMessageDelete={onMessageDelete}
                    onMessageEdit={handleMessageEdit}
                  />
                ) : (
                  <Message
                    key={id}
                    id={id}
                    currentUserId={currentUserId}
                    avatar={avatar}
                    user={user}
                    text={text}
                    createdAt={createdAt}
                    editedAt={editedAt}
                    isLiked={liked}
                    onMessageLike={handleMessageLike}
                  />
                )
            )}
          </ul>
        );
      })}

      {hasCurrentMessage && (
        <EditMessageModal
          message={currentMessage}
          onSave={handleMessageSave}
          onClose={handleEditMessageModalClose}
        />
      )}
    </section>
  );
}

export default MessageList;
