import { getTime } from '../../../Helpers/getTime';
import { useEffect, useState } from 'react';

const Message = ({ user, id, currentUserId, avatar, text, editedAt, createdAt, isLiked, onMessageLike }) => {

    const [ isMessageLiked, setIsMessageLiked ] = useState(false);


    useEffect(() => {
        setIsMessageLiked(isLiked.includes(currentUserId));
    }, [ isLiked ])


    const onLikedMessage = () => {
        onMessageLike(id)
    };

    return (
        <>
            <li className='message flex m-4 rounded-md w-2/3 justify-between items-center bg-lime-100 p-2 '>
                <div className='flex flex-col justify-center items-center border-r-2 p-4'>
                    <img className='w-10 h-10 rounded-full ' src={ avatar } alt='user-avatar'/>
                    <span className='message-user-name'>
                { user }
            </span>
                </div>
                <div className='text-right pl-4'>
                    <span className='message-text'>{ text }</span>
                    <div
                        className='message-time text-right opacity-50 mt-4'>{ editedAt ? `edited at: ${ getTime(editedAt) }` : `sent at: ${ getTime(createdAt) }` }</div>
                    <button onClick={ onLikedMessage } className='inline message-like w-10 h-10 text-2xl rounded'>{
                        isMessageLiked
                            ? <span className='text-red-400 hover:text-red-600'>&hearts;</span>
                            : <span className='text-black hover:text-red-600'>&#9829;</span> }</button>
                </div>
            </li>
        </>
    );
};

export default Message;