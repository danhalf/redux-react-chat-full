import { useCallback, useEffect, useState } from 'react';
import Header from '../Header/Header';
import MessageList from '../MessageList/MessageList';
import { Loader } from '../common/common';
import { messages as messagesActionCreator, } from '../../store/actions';
import MessageInput from '../MessageInput/MessageInput';
import { useDispatch, useSelector } from 'react-redux';
import { DataStatus } from '../../common/enums/enums';

const Chat = () => {
    const userId = 'uid';
    const { messages, preloader } = useSelector(({ messages }) => ({
        messages: messages.messages,
        preloader: messages.preloader,
    }));
    const [ messagesCount, setMessagesCount ] = useState(0);
    const [ uniqUsersCount, setUniqUsersCount ] = useState(0);
    //   const [isLoaded, setIsLoaded] = useState(false);

    const dispatch = useDispatch();

    const handleMessageAdd = useCallback(
        (message) => {
            dispatch(messagesActionCreator.addMessage(message));
        },
        [ dispatch ]
    );

    const handleMessageDelete = useCallback(
        (message) => {
            dispatch(messagesActionCreator.deleteMessage(message));
        },
        [ dispatch ]
    );

    useEffect(() => {
        dispatch(messagesActionCreator.fetchMessages());
    }, [ dispatch ]);

    useEffect(() => {
        setMessagesCount(messages?.length);
        setUniqUsersCount(
            [ ...new Set(messages?.map(({ userId }) => userId)) ].length
        );
    }, [ messages ]);

    if (preloader === DataStatus.PENDING) {
        return <Loader/>;
    }

    return (
        <>
            <Header uniqUsersCount={ uniqUsersCount } messagesCount={ messagesCount }/>
            <MessageList
                messages={ messages }
                onMessageDelete={ handleMessageDelete }
                currentUserId={ userId }
            />
            <MessageInput onMessageAdd={ handleMessageAdd }/>
        </>
    );
};

export default Chat;
