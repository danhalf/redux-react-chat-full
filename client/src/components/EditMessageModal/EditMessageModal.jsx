import Modal from '../common/Modal/Modal';
import { useState } from 'react';

const EditMessageModal = ({ message, onSave, onClose }) => {
  const [ currentMessage, setCurrentMessage ] = useState(message);
  const { text } = currentMessage;
  const handleChange = ({ target }) => {
    const { value } = target;
    setCurrentMessage({ ...currentMessage, text: value });
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();
    onSave(currentMessage);
  };

  return (
    <Modal isOpen={ Boolean(currentMessage) } onClose={ onClose }>
      <div
        className='edit-message-modal modal-shown bg-slate-800 bg-opacity-50 flex justify-center items-center absolute top-0 right-0 bottom-0 left-0'>
        <form onSubmit={ handleSubmit } className='bg-white px-16 py-14 rounded-md text-center'>
          <textarea className='w-full border-2 resize-none' value={ text } rows='4' onChange={ handleChange } />
          <button onClick={ onClose } className='bg-indigo-500 px-4 py-2 rounded-md text-md text-white'>
            Close
          </button>
          <button type='submit' className='bg-red-500 px-7 py-2 ml-2 rounded-md text-md text-white font-semibold'>
            Edit
          </button>
        </form>
      </div>
    </Modal>

  );
};


export default EditMessageModal;
