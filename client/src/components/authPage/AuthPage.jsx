import { useState, useEffect, useCallback } from 'react';
import { users as usersActionCreator } from '../../store/actions';
import { useDispatch } from 'react-redux';
import Login from "./Login/Login";
import Registration from "./Registration/Registration";

const AuthPage = () => {
    const dispatch = useDispatch();
    const [ isRegister, setIsRegister ] = useState(true);

    useEffect(() => {
        dispatch(usersActionCreator.fetchUsers());
    }, [ dispatch ]);

    const handleRegisterChange = (event) => {
        event.preventDefault();
        return setIsRegister(!isRegister)
    }

    const handleUserAdd = useCallback(
        (user) => {
            dispatch(usersActionCreator.addUser(user));
        },
        [ dispatch ]
    );
    const handleLoginSend = useCallback(
        (user) => {
            dispatch(usersActionCreator.getUser(user));
        },
        [ dispatch ]
    );

    return isRegister
        ? <Login onRegisterChange={ handleRegisterChange } onLoginDataSend={ handleLoginSend }/>
        : <Registration onRegisterChange={ handleRegisterChange } onUserDataSend={ handleUserAdd }/>
};

export default AuthPage;
