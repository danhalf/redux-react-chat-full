import React, { useState } from "react";

const Login = ({onRegisterChange, onLoginDataSend}) => {
    const [user, setUser] = useState('');
    const [password, setPassword] = useState('');
    const [isDisable, setIsDisable] = useState(true)

    const changeUserName = ({ target }) => {
        const userName = target.value;
        setUser(userName);
        checkIsDisabled(user && password)
    }

    const changePassword = ({ target }) => {
        const userPassword = target.value;
        setPassword(userPassword);
        checkIsDisabled(user && password)
    }

    const checkIsDisabled = (value) => value ? setIsDisable(false) : setIsDisable(true)

    const handleUserDataSend = () => {
        if (user && password) {
            onLoginDataSend({ user, password })
            setUser('');
            setPassword('');
            setIsDisable(true)
        }
    }
    return (
        <div className="w-full flex justify-center items-center h-screen">
            <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                <div className="mb-4">
                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="username">
                        Username
                    </label>
                    <input
                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        id="username" value={user} onChange={changeUserName} type="text" placeholder="Username"/>
                </div>
                <div className="mb-6">
                    <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                        Password
                    </label>
                    <input
                        className="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                        id="password" value={password} onChange={changePassword} type="password" placeholder="******************"/>
                    <p className="text-red-500 text-xs italic">Please choose a password.</p>
                </div>
                <div className="flex items-center justify-between">
                    <button
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none disabled:bg-gray-300 mr-4 focus:shadow-outline"
                        type="button" disabled={isDisable} onClick={handleUserDataSend}>
                        Sign In
                    </button>
                    <button onClick={ onRegisterChange }
                            className="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800">
                        Create account
                    </button>
                </div>
            </form>
        </div>
    );
};

export default Login;