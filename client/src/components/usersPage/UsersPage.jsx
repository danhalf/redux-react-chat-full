import { useEffect } from 'react';
import { users as usersActionCreator, } from '../../store/actions';
import { useDispatch, useSelector } from 'react-redux';

const UsersPage = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(usersActionCreator.fetchUsers());
    }, [ dispatch ]);

    const { users } = useSelector(({ users }) => ({
        users: users.users,
    }));


    return (
        <ul className="page__users users list-none">
            { users.map(({ userId, user: userName, role }) =>
                <li key={ userId } className="p-2 users__item flex w-24 min-w-full justify-between gap-4">
                    <span className="flex-1 users__item--name w-25">{ userName }</span>
                    <span className="flex-1 users__item--role">{ role }</span>
                    <button
                        className="flex-1 user__edit bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                        Edit
                    </button>
                    <button
                        className="flex-1  user__delete bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow">
                        Delete
                    </button>
                </li>) }
        </ul>
    );
};

export default UsersPage;
