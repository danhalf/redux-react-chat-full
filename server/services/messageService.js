const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {

    getMessage() {
        try {
            return MessageRepository.getAll();
        } catch (error) {
            throw Error(error);
        }
    }

    searchMessage(search) {
        const item = MessageRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    create(message) {
        const isUserCreate = MessageRepository.create(message);
        if (!isUserCreate) {
            throw Error('Message was not created');
        }
        return message;
    }

    changeMessage(id, message) {
        const searchedMessage = this.searchMessage({id})
        const isMessageUpdate = searchedMessage && MessageRepository.update(id, message);
        if (!isMessageUpdate) {
            throw Error('Message was not updated');
        }
        return message;
    }

    likeMessage(id, userId) {
        const searchedMessage = this.searchMessage({id})
        const isMessageUpdate = searchedMessage && MessageRepository.like(id, userId);
        if (!isMessageUpdate) {
            throw Error('Message was not updated');
        }
        return message;
    }



    deleteMessage(id) {
        const searchedMessage = this.searchMessage({id})
        const isMessageUpdate = searchedMessage && MessageRepository.delete(id);
        if (!isMessageUpdate) {
            throw Error('Message not deleted');
        }
        return id;
    }
}

module.exports = new MessageService();