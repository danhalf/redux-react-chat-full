const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get(
    '/',
    (req, res, next) => {
        try {
            if (!res.err) res.send(MessageService.getMessage(res.body));
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);
router.get(
    '/:id',
    (req, res, next) => {
        const { id } = req.params;
        try {
            if (!res.err) res.send(MessageService.searchMessage({ id }));
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

router.post(
    '/',
    (req, res, next) => {
        try {
            if (!res.err) res.data = MessageService.create(req.body);
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

router.put(
    '/:id',
    (req, res, next) => {
        try {
            const { id } = req.params;
            (!res.err && req.body.liked)
                ? res.data = MessageService.likeMessage(id, req.body)
                : res.data = MessageService.changeMessage(id, req.body)

        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

router.delete(
    '/:id',
    (req, res, next) => {
        try {
            const { id } = req.params;
            if (!res.err) res.data = MessageService.deleteMessage(id);
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

module.exports = router;