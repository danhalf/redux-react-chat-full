const userRoutes = require('./userRoutes');
const authRoutes = require('./authRoutes');
const messagesRoutes = require('./messagesRoutes');

module.exports = (app) => {
    app.use('/api/users', userRoutes);
    app.use('/api/messages', messagesRoutes);
    app.use('/api/auth', authRoutes);
  };