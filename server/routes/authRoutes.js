const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
  '/login',
  (req, res, next) => {
      const message = 'entered data is not correct'
    try {
        const { email, password } = req.body
        const userData = AuthService.login({ email, password })
        if (!userData) {
            res.data.status(400).send({ error: true, message })
        }
        res.data = userData;
    } catch (err) {
      res.err = err;
        next(message);
    } finally {
        next();
    }
  },
  responseMiddleware
);

module.exports = router;
