const { search } = require("../../services/userService");

function isRequestBodyCorrect({user, password}) {
  if (!user || !password)
    return `request body must be filled`;
  const isNameAlreadyExists = search({ user });
  if (isNameAlreadyExists) return `user with name: '${ user }' has already been created`;
  return false
}

module.exports = {
  isRequestBodyCorrect
};
