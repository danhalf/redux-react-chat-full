const {
  isRequestBodyCorrect
} = require('./validation/userValidation')

const isUserValid = (req, res, next, put = false) => {
    const request = req.body;

    const validationError = message => {
        if (message !== false) {
            res.status(400).json({ error: true, message })
            next(message)
        }
    }
    if (put) {
        validationError(isRequestBodyCorrect({ ...request }))
    } else {
        validationError(isRequestBodyCorrect(request))
    }
    next();
}

const createUserValid = (req, res, next) => isUserValid(req, res, next)
const updateUserValid = (req, res, next) => isUserValid(req, res, next, true)


exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
