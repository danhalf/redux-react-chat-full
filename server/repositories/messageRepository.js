const { BaseRepository } = require('./baseRepository');

class MessageRepository extends BaseRepository {
    constructor() {
        super('messages');
    }

    like(id, dataToUpdate) {
        dataToUpdate.id = id;
        const {liked} = dataToUpdate;
        return this.dbContext.find({ id }).get('liked').push(liked).write();
    }
}

exports.MessageRepository = new MessageRepository();